# three-snowman

This is the ES6 and refactorized version of my three.js tryout which you can found at [https://codepen.io/RobinDevouge/pen/rWGRjV](https://codepen.io/RobinDevouge/pen/rWGRjV)

I made this as an intro exercise to ES6