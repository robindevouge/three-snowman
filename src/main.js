import ThreeScene from 'threeScene';
import Snowman from 'snowman';
import Snow from 'snow';
import Floor from 'floor';

const studio = new ThreeScene();
const snowman = new Snowman();
const mini = new Snowman();
const snow = new Snow();
const floor = new Floor();

studio.add(snowman, 'snowman');
studio.add(mini, 'mini');
mini.mesh.translateX(-500);
mini.mesh.translateZ(300);
const miniScale = 0.3;

mini.mesh.scale.set(miniScale, miniScale, miniScale);
studio.add(snow, 'snow');
studio.add(floor, 'floor');
studio.render();

console.info('Ready! 🚀');
