import * as THREE from 'three';

class Snowman {
  constructor() {
    this.initMaterials();
    this.initMeshes();
    this.groupMeshes();
    this.placeMeshes();
    this.enableMeshShadows()
    this.scarfPosY = [0, 10, 20, 30];
    this.armWaveBackwards = false;
  }
  initMaterials() {
    this.materials = {
      white: new THREE.MeshLambertMaterial({
        color: 0xffffff,
      }),
      black: new THREE.MeshLambertMaterial({
        color: 0x212121,
      }),
      red: new THREE.MeshLambertMaterial({
        color: 0xef5350,
      }),
      orange: new THREE.MeshLambertMaterial({
        color: 0xffb74d,
      }),
      brown: new THREE.MeshLambertMaterial({
        color: 0x8d6e63,
      }),
    }
  }
  initMeshes() {
    this.dim = {
      base: 400,
      torso: 300,
      head: 225,
      arm: [250, 20, 20],
      button: [35, 35, 10],
      scarfNeck: [270, 60, 270],
      scarfPart: [100, 10, 70],
      mouthPart: [20, 20, 10],
      noseBase: [25, 25, 35],
      noseMid: [20, 20, 35],
      noseEnd: [15, 15, 35],
      eye: [35, 35, 10],
      crown: [150, 200, 150],
      brim: [275, 15, 275],
      band: [151, 30, 151],
    }
    this.mesh = new THREE.Group();
    this.parts = {
      // Balls
      snowBall: {
        base: new THREE.Mesh(this.cube(this.dim.base), this.materials.white),
        torso: new THREE.Mesh(this.cube(this.dim.torso), this.materials.white),
        head: new THREE.Mesh(this.cube(this.dim.head), this.materials.white),
      },
      // TORSO
      torso: new THREE.Group(),
      // Arms
      rightShoulder: new THREE.Group(),
      rightArm: new THREE.Mesh(this.box(this.dim.arm), this.materials.brown),
      rightElbow: new THREE.Group(),
      rightForearm: new THREE.Mesh(this.box(this.dim.arm), this.materials.brown),
      leftShoulder: new THREE.Group(),
      leftArm: new THREE.Mesh(this.box(this.dim.arm), this.materials.brown),
      leftElbow: new THREE.Group(),
      leftForearm: new THREE.Mesh(this.box(this.dim.arm), this.materials.brown),
      // Buttons
      button: new THREE.Group(),
      buttonPart: [
        new THREE.Mesh(this.box(this.dim.button), this.materials.black),
        new THREE.Mesh(this.box(this.dim.button), this.materials.black),
        new THREE.Mesh(this.box(this.dim.button), this.materials.black),
      ],
      // HEAD
      head: new THREE.Group(),
      // Scarf
      scarf: new THREE.Group(),
      scarfNeck: new THREE.Mesh(this.box(this.dim.scarfNeck), this.materials.red),
      scarfTail: new THREE.Group(),
      scarfPart: [
        new THREE.Mesh(this.box(this.dim.scarfPart), this.materials.red),
        new THREE.Mesh(this.box(this.dim.scarfPart), this.materials.red),
        new THREE.Mesh(this.box(this.dim.scarfPart), this.materials.red),
        new THREE.Mesh(this.box(this.dim.scarfPart), this.materials.red),
      ],
      // Mouth
      mouth: new THREE.Group(),
      mouthPart: [
        new THREE.Mesh(this.box(this.dim.mouthPart), this.materials.black),
        new THREE.Mesh(this.box(this.dim.mouthPart), this.materials.black),
        new THREE.Mesh(this.box(this.dim.mouthPart), this.materials.black),
        new THREE.Mesh(this.box(this.dim.mouthPart), this.materials.black),
        new THREE.Mesh(this.box(this.dim.mouthPart), this.materials.black),
      ],
      // Nose
      nose: new THREE.Group(),
      noseBase: new THREE.Mesh(this.box(this.dim.noseBase), this.materials.orange),
      noseMid: new THREE.Mesh(this.box(this.dim.noseMid), this.materials.orange),
      noseEnd: new THREE.Mesh(this.box(this.dim.noseEnd), this.materials.orange),
      // Eyes
      eyes: new THREE.Group(),
      eyeRight: new THREE.Mesh(this.box(this.dim.eye), this.materials.black),
      eyeLeft: new THREE.Mesh(this.box(this.dim.eye), this.materials.black),
      // Hat
      hat: new THREE.Group(),
      crown: new THREE.Mesh(this.box(this.dim.crown), this.materials.black),
      brim: new THREE.Mesh(this.box(this.dim.brim), this.materials.black),
      band: new THREE.Mesh(this.box(this.dim.band), this.materials.red),
    }
  }
  groupMeshes() {
    // Mesh
    this.mesh.add(this.parts.snowBall.base);
    this.mesh.add(this.parts.torso);
    this.mesh.add(this.parts.head);
    // Torso
    this.parts.torso.add(this.parts.snowBall.torso);
    this.parts.torso.add(this.parts.leftShoulder);
    this.parts.leftShoulder.add(this.parts.leftArm);
    this.parts.leftShoulder.add(this.parts.leftElbow);
    this.parts.leftElbow.add(this.parts.leftForearm);
    this.parts.torso.add(this.parts.rightShoulder);
    this.parts.rightShoulder.add(this.parts.rightArm);
    this.parts.rightShoulder.add(this.parts.rightElbow);
    this.parts.rightElbow.add(this.parts.rightForearm);
    this.parts.torso.add(this.parts.button);
    this.parts.button.add(this.parts.buttonPart[0]);
    this.parts.button.add(this.parts.buttonPart[1]);
    this.parts.button.add(this.parts.buttonPart[2]);
    // Head
    this.parts.head.add(this.parts.snowBall.head);
    this.parts.head.add(this.parts.scarf);
    this.parts.scarf.add(this.parts.scarfNeck);
    this.parts.scarf.add(this.parts.scarfTail);
    this.parts.scarfTail.add(this.parts.scarfPart[0]);
    this.parts.scarfTail.add(this.parts.scarfPart[1]);
    this.parts.scarfTail.add(this.parts.scarfPart[2]);
    this.parts.scarfTail.add(this.parts.scarfPart[3]);
    this.parts.head.add(this.parts.mouth);
    this.parts.mouth.add(this.parts.mouthPart[0]);
    this.parts.mouth.add(this.parts.mouthPart[1]);
    this.parts.mouth.add(this.parts.mouthPart[2]);
    this.parts.mouth.add(this.parts.mouthPart[3]);
    this.parts.mouth.add(this.parts.mouthPart[4]);
    this.parts.head.add(this.parts.nose);
    this.parts.nose.add(this.parts.noseBase);
    this.parts.nose.add(this.parts.noseMid);
    this.parts.nose.add(this.parts.noseEnd);
    this.parts.head.add(this.parts.eyes);
    this.parts.eyes.add(this.parts.eyeLeft);
    this.parts.eyes.add(this.parts.eyeRight);
    this.parts.head.add(this.parts.hat);
    this.parts.hat.add(this.parts.crown);
    this.parts.hat.add(this.parts.brim);
    this.parts.hat.add(this.parts.band);
  }
  placeMeshes() {
    this.mesh.translateX(0);
    // Base
    this.parts.snowBall.base.translateY(200);
    // Torso
    this.parts.torso.translateY(550);
    // Left arm
    this.parts.leftShoulder.translateX(140);
    this.parts.leftShoulder.translateY(50);
    this.parts.leftShoulder.rotation.z = Math.PI / 8;
    this.parts.leftArm.translateX(125);
    this.parts.leftElbow.translateX(240);
    this.parts.leftElbow.rotation.z = (Math.PI / 4) + (Math.PI / 60);
    this.parts.leftForearm.translateX(115);
    // Right arm
    this.parts.rightShoulder.translateX(-140);
    this.parts.rightShoulder.translateY(50);
    this.parts.rightShoulder.rotation.z = Math.PI / 4;
    this.parts.rightArm.translateX(-125);
    this.parts.rightElbow.translateX(-240);
    this.parts.rightElbow.rotation.z = Math.PI / 4;
    this.parts.rightForearm.translateX(-115);
    // Buttons
    this.parts.button.translateZ(155);
    this.parts.buttonPart[0].translateY(-80);
    this.parts.buttonPart[2].translateY(80);
    // Head
    this.parts.head.translateY(812);
    // Scarf
    this.parts.scarf.translateY(-90);
    this.parts.scarfTail.translateX(-180);
    this.parts.scarfTail.translateZ(40);
    this.parts.scarfTail.rotation.z = Math.PI / 20;
    this.parts.scarfPart[1].translateX(-100);
    this.parts.scarfPart[1].position.y = 10;
    this.parts.scarfPart[2].translateX(-200);
    this.parts.scarfPart[2].position.y = 20;
    this.parts.scarfPart[3].translateX(-300);
    this.parts.scarfPart[3].position.y = 30;
    // Mouth
    this.parts.mouth.translateY(-60);
    this.parts.mouth.translateZ(115);
    this.parts.mouthPart[0].translateX(-60);
    this.parts.mouthPart[0].translateY(25);
    this.parts.mouthPart[1].translateX(-30);
    this.parts.mouthPart[1].translateY(5);
    this.parts.mouthPart[3].translateX(30);
    this.parts.mouthPart[3].translateY(5);
    this.parts.mouthPart[4].translateX(60);
    this.parts.mouthPart[4].translateY(25);
    // Nose
    this.parts.nose.translateZ(130);
    this.parts.noseMid.translateZ(35);
    this.parts.noseEnd.translateZ(70);
    // Eyes
    this.parts.eyes.translateY(50);
    this.parts.eyes.translateZ(115);
    this.parts.eyeLeft.translateX(50);
    this.parts.eyeRight.translateX(-50);
    // Hat
    this.parts.hat.translateY(200);
    this.parts.brim.translateY(-90);
    this.parts.band.translateY(-50);
  }
  enableMeshShadows() {
    this.mesh.traverse((object) => {
      if (object instanceof THREE.Mesh) {
        object.castShadow = true;
        object.receiveShadow = true;
      }
    });
  }
  scarfWave() {
    for (let i = 0; i < this.parts.scarfPart.length; i++) {
      const seg = this.parts.scarfPart[i];

      seg.position.y = 7 * Math.sin(this.scarfPosY[i] * 3);
      this.scarfPosY[i] += 0.07;
    }
  }
  armWave(rotator, maxAngle, minAngle, speed) {
    const direction = Math.PI / speed;
    let rotation = rotator.rotation.z;

    if (rotation > maxAngle) {
      this.armWaveBackwards = true
    }
    if (rotation < minAngle) {
      this.armWaveBackwards = false
    }

    if (this.armWaveBackwards) {
      rotation -= direction;
    } else {
      rotation += direction;
    }
    rotator.rotation.z = rotation;
  }
  cube(radius) {
    return new THREE.BoxBufferGeometry(radius, radius, radius);
  }
  box(dim) {
    const [x, y, z] = dim;

    return new THREE.BoxBufferGeometry(x, y, z);
  }
}

export default Snowman;
