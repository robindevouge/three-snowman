import * as THREE from 'three';

class Floor {
  constructor() {
    const floorMaterial = new THREE.MeshLambertMaterial({
      color: 0xffffff,
    })

    this.mesh = new THREE.Mesh(
      new THREE.PlaneGeometry(4000, 4000),
      floorMaterial
    );
    this.mesh.rotation.x = -Math.PI / 2;
    this.mesh.receiveShadow = true;
  }
}

export default Floor;
