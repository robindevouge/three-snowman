import * as THREE from 'three';
import OrbitControls from 'orbit-controls-es6';

class ThreeScene {
  constructor() {
    this.objects = {};
    this.initRenderer();
    this.initCamera();
    this.initLights();
    this.initControls();
    this.initListeners();
    this.clock = new THREE.Clock(true);
  }
  initRenderer() {
    this.renderer = new THREE.WebGLRenderer({
      alpha: true,
      antialias: true,
    });
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    document.getElementById('container').appendChild(this.renderer.domElement);

    this.scene = new THREE.Scene();
  }
  initCamera() {
    this.camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 1, 10000);
    this.camera.position.set(500, 1000, 2000);
    this.camera.lookAt(new THREE.Vector3(0, 800, 0));
    this.scene.add(this.camera);
  }
  initLights() {
    this.frontLight = new THREE.SpotLight(0xffffff, 0.6);
    this.frontLight.position.set(700, 1500, 1500);
    this.frontLight.position.multiplyScalar(1.45); // Puts light further but with the same angle

    this.frontLight.castShadow = true;
    this.shadowMapSize = 2048;
    this.frontLight.shadow.mapSize.width = this.shadowMapSize;
    this.frontLight.shadow.mapSize.height = this.shadowMapSize;

    const d = 1200;

    this.frontLight.shadow.camera.left = -d;
    this.frontLight.shadow.camera.right = d;
    this.frontLight.shadow.camera.top = d;
    this.frontLight.shadow.camera.bottom = -d;

    this.frontLight.shadow.camera.far = 5000;



    this.backLight = new THREE.DirectionalLight(0xffffff, 0.6);
    this.backLight.position.set(-700, 3000, -3000);

    this.ambLight = new THREE.AmbientLight(0x474747);

    this.light = new THREE.HemisphereLight(0xffffdd, 0xffffdd, 0.2)


    this.scene.add(this.frontLight);
    this.scene.add(this.backLight);
    this.scene.add(this.ambLight);
    this.scene.add(this.light);
  }
  initControls() {
    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    this.controls.target = new THREE.Vector3(0, 500, 0);
    this.controls.update();
  }
  initListeners() {
    window.addEventListener('resize', () => {
      this.onResize();
    });
  }
  onResize() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.renderer.setSize(this.width, this.height);
    this.camera.aspect = this.width / this.height;
    this.camera.updateProjectionMatrix();
  }
  add(obj, key) {
    this.objects[key] = obj;
    this.scene.add(obj.mesh);
  }
  render() {
    requestAnimationFrame(() => {
      this.render();
    });

    const deltaTime = this.clock.getDelta();
    const {
      snowman,
      snow,
      mini,
    } = this.objects;

    snowman.scarfWave();
    snowman.armWave(snowman.parts.leftShoulder, Math.PI / 4, Math.PI / 30, 180);
    snowman.armWave(snowman.parts.leftElbow, Math.PI / 3, Math.PI / 10, 60);
    mini.scarfWave();
    snow.fall(deltaTime, 300);

    this.renderer.render(this.scene, this.camera);
  }
}

export default ThreeScene;
