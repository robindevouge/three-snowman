import * as THREE from 'three';

class Snow {
  constructor() {
    const snowMaterial = new THREE.PointsMaterial({
      color: 0xffffff,
      size: 10,
    });

    this.particleCount = 10000;
    this.particles = new THREE.Geometry();

    this.initParticles();

    this.mesh = new THREE.Points(this.particles, snowMaterial);
  }
  initParticles() {
    for (let p = 0; p < this.particleCount; p++) {
      const x = (Math.random() * 4000) - 2000;
      const y = Math.random() * 2000;
      const z = (Math.random() * 4000) - 2000;

      const particle = new THREE.Vector3(x, y, z);

      this.particles.vertices.push(particle);
    }
  }
  fall(deltaTime, speed) {
    const verts = this.mesh.geometry.vertices;

    for (let i = 0; i < verts.length; i++) {
      const vert = verts[i];

      if (vert.y < 0) {
        vert.y = Math.random() * 2000;
      }
      vert.y -= speed * deltaTime;
      if (vert.x < -2000) {
        vert.x = Math.random() * 2000;
      }
      vert.x -= speed * 2 * deltaTime;
    }
    // Necessary to tell threeJS the geometry has changed and needs to be refreshed
    this.mesh.geometry.verticesNeedUpdate = true;
  }
}

export default Snow;
